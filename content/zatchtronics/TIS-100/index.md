---
Title: TIS-100
Description: Juegos de Zatchtronics
Author: kreilly
Date: 2018-09-25
Template: index
---

## TIS-100

![TIS-100](%base_url%/assets/zatchtronics/TIS-100/TIS-100.png)

[Zatchtronics](http://www.zachtronics.com/tis-100/) | [Steam](https://store.steampowered.com/app/370360/TIS100)

1. [SELF-TEST DIAGNOSTIC](?zatchtronics/TIS-100/1)
1. [SIGNAL AMPLIFIER](?zatchtronics/TIS-100/2)
3. [DIFFERENTIAL CONVERTER](?zatchtronics/TIS-100/3)
4. [SIGNAL COMPARATOR](?zatchtronics/TIS-100/4)
5. [SIGNAL MULTIPLEXER](?zatchtronics/TIS-100/5)
6. [SEQUENCE GENERATOR](?zatchtronics/TIS-100/6)
7. [SEQUENCE COUNTER](?zatchtronics/TIS-100/7)
8. [SIGNAL EDGE DETECTOR](?zatchtronics/TIS-100/8)




