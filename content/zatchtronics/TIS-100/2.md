---
Title: SIGNAL AMPLIFIER
Description: SIGNAL AMPLIFIER
Author: kreilly
Date: 2018-09-25
Template: index
---

2. SIGNAL AMPLIFIER

![SIGNAL AMPLIFIER](%base_url%/assets/zatchtronics/TIS-100/2.png)

```
@0


@1
##TONTUNETAS
MOV UP, ACC
ADD ACC
MOV ACC, DOWN

@2


@3


@4
MOV UP, RIGHT

@5
MOV LEFT, DOWN

@6


@7


@8
MOV UP, DOWN

@9
```