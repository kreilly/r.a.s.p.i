---
Title: SEQUENCE COUNTER
Description: SEQUENCE COUNTER
Author: kreilly
Date: 2018-09-27
Template: index
---

7. SEQUENCE COUNTER

![SEQUENCE COUNTER](%base_url%/assets/zatchtronics/TIS-100/7.png)

```
@0
##ESTO MEJORA

@1
MOV UP ACC
MOV ACC RIGHT
MOV ACC DOWN

@2
MOV LEFT ACC
JNZ RUN
JEZ RESET
RUN: SWP
ADD 1
SAV
JRO -6
RESET:
SWP
MOV ACC DOWN
JRO -10

@3
MOV RIGHT RIGHT

@4
MOV UP ACC
JEZ RESET
MOV ACC LEFT
SWP
ADD LEFT
SAV
JRO -99
RESET:
SWP
MOV ACC DOWN
JRO -99

@5
MOV UP DOWN

@6


@7


@8
MOV UP DOWN

@9
MOV UP DOWN

@10



```












