---
Title: arduino
Description: yo qué sé ya
Author: kreilly
Date: 2018-09-21
Template: index
---

# [Arduino](https://www.arduino.cc)

[Traducción](arduino/trad) de *arduino programming notebook*, de Brian W. Evans. |[epub](%base_url%/assets/arduino/pn/apa.epub)| [original](https://playground.arduino.cc/uploads/Main/arduino_notebook_v1-1.pdf) |
