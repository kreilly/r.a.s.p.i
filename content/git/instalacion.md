---
Title: git instalación
Description:
Author: kreilly
Date: 2018-12-04
Template: index
---

## Instalación.

Este capítulo no me interesa demasiado, porque puede ser que los procesos para instalar el programa vayan cambiando a lo largo del tiempo. Pero no está mal dejar una breve guía de algo que, de todos modos, es breve.

### Windows y MacOs.

Visita la sección de descargas de la web de [git](https://git-scm.com/downloads) para elegir el archivo de instalación y ejecútalo. La primera vez deja las opciones que te va preguntando en las que ya aparecen seleccionadas por defecto, si con el uso vas prefiriendo otras opciones siempre puedes cambiarlas.

### Linux.

Las instrucciones para instalarlo están en [esta dirección](https://git-scm.com/download/linux), aunque normalmente ya estará instalado en tu distribución. Puedes saber si ya lo tienes abriendo un terminal y tecleando

```
$ git --version
```

Si la respuesta no es un mensaje de error, te dirá qué versión tienes instalada.
