---
Title: git
Description:
Author: kreilly
Date: 2019-01-03
Template: index
---

# [Git](https://git-scm.com/)


| [apuntes](git/apuntes) | [intro](git/intro) | [instalación](git/instalacion) | [configuración](git/configuracion) | [terminal](git/terminal) | [empezando](git/empezando)