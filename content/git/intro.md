---
Title: git
Description:
Author: kreilly
Date: 2018-12-04
Template: index
---

## Introducción.

### Los buenos manuales.

Es un poco extraño empezar un manual sobre cualquier cosa hablando de otros, pero mi intención con lo que tienes delante es iniciarte en un uso de git para proyectos **no** relacionados con la programación. Eso, seguramente, para el que quiera seguir avanzando en la herramienta, se quedará corto. Y está bien que sea así.

El mejor manual de git en español es [el propio de git](https://git-scm.com/book/es/v2). Puede descargarse en .epub, .pdf o .mobi en la misma página.

El [manual](https://github.com/JJ/aprende-git) de JJ Merelo es también una opción muy interesante para aquellos que quieran aprender a utilizar git en su uso con github. Yo hablaré de repositorios remotos, pero utilizaré gitlab.

### Por qué un control de versiones en un proyecto creativo cualquiera.

Nunca supe que era un sistema de control de versiones, pero aún así utilizaba uno. Se llama generalmente *ir metiéndolo todo en diferentes carpetas*. Como no soy una persona muy organizada, las carpetas del interior se empezaban a llamar "versión 1", "versión 1 nueva", "versión 1 nuevo personaje", "versión 1 definitiva", "versión 1 definitiva final" y... bueno, creo que es fácil hacerse una idea de lo complicado que resultaba, llegado el momento, encontrar algo en concreto. Pero es una buena ilusión a la hora de convencerte que no has perdido nada.

Un sistema de control de versiones, sin embargo, genera fotografías de los momentos que tú eliges, y te permite volver a ellas fácilmente. En git cuando vuelves atrás en el tiempo no lo haces a través de un programa de visualización, sino que tus archivos cambian y se convierten en lo que tenías entonces. Cuando digo cambian es que si mantienes abierta la carpeta en el explorador mientras saltas hacia cualquier parte verás cómo tus archivos se convierten en los que eran, literalmente. Tu carpeta, y su contenido, vuelve a ser la misma que la del momento determinado al que decides volver.

Eso casi hace que me explote la cabeza la primera vez que lo vi en funcionamiento. Era el sueño de un desorganizado (y supongo que de el de alguien organizado también, y por lo mismo).

La transformación que git hace de los archivos puede ser de dos modos, uno el que ya he comentado, una vuelta atrás en el tiempo, y otro a través de las ramas. Las ramas son dimensiones paralelas. Cuando quieres probar con un cambio de tono o perspectiva en un capítulo en concreto puedes abrir una nueva rama en la que ese capítulo exista, puedes seguir avanzando en el desarrollo en ambas ramas y mantenerlas en el tiempo, y eso en un solo archivo que muta cuando se lo pides y se convierte en lo que necesitas. Más tarde puedes fusionar ambas ramas para quedarte con la que prefieres, sin perder ninguna de ellas por separado en tu libro de viaje. Las fotografías del pasado siguen ahí, puedes volver a ellas cuando quieras.

Es decir, como resumen, puedes tener un sólo archivo o muchos con todo tu trabajo, mejorarlo a lo largo del tiempo en una sucesión temporal o paralela y guardar imágenes de cada paso, abrir dimensiones alternativas en las que las cosas suceden de otra manera, y saltar en el tiempo y entre las dimensiones de un modo fácil e inmediato.

Cómo no iba a explotarme la cabeza.

Utilizaré probablemente [la chuleta](https://about.gitlab.com/images/press/git-cheat-sheet.pdf) de Gitlab como guía (o no).

El desarrollo del manual estará en el [repositorio de gitlab](https://gitlab.com/kreilly/r.a.s.p.i) donde está el resto de la web.
