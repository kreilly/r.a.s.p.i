---
Title: git terminal
Description:
Author: kreilly
Date: 2018-12-04
Template: index
---

## Terminal

El terminal no es el único modo, hay interfaces gráficas (GUI) para hacer lo mismo, y supongo que en algún momento hablaré de ellas. Pero, realmente, no es tanto lo que hay que aprender y creo que el terminal es la forma más sencilla y potente de manejar git.

En Windows podemos hacer botón derecho dentro de nuestra carpeta y seleccionar "git bash here", en MacOS finder/services/new terminal at folder, en Linux dependiendo de la distribución nos aparecerá también en el botón derecho. Una vez abierto, puedes continuar a [empezando](empezando).

Si no sabes moverte por los archivos en el terminal y quieres aprender lo básico para hacerlo, sólo necesitas tres comandos:

El primero es

```
$ pwd
```

Significa print working directory, es decir, enséñame dónde estoy. Una vez que sabemos dónde estamos, utilizaremos

```
$ ls
```

Significa "list", lista (pero como imperativo, lístame) y nos va a enseñar todos los archivos de la carpeta en la que estamos. Veremos los archivos y las carpetas que están dentro de la carpeta en la que estamos. Cuando sepamos a dónde queremos ir, utilizaremos

```
$ cd la-carpeta-a-la-que-voy
```

cd significa change directory, cambia de directorio. Es decir, si nuestro proyecto está en la carpeta "escrito" teclaremos

```
$ cd escrito
```

Pensad en el sistema de archivos en general, ¿qué es? Es un tronco del que salen ramas. En windows y mac hay un arbol por cada unidad de almacenamiento (disco duro) que haya. En windows podemos tener, por ejemplo, dos discos duros.

C:\  
D:\

Y de cada uno de esos troncos van saliendo las ramas del resto de los archivos. Cuando hacemos doble click en una de las carpetas de C:\, por ejemplo en "miusuario", es como si escribiéramos en el terminal `cd miusuario`, y cuando entramos y vemos lo que hay dentro es como si hubiéramos tecleado `ls` después de entrar. "C:\Users\k\Documents\MEGA\escribiendo" es una la rama escribiendo que sale de MEGA, que sale de Documents, que sale de K, que sale de users, que sale de C.

```
$ pwd //dónde estoy
$ ls //lístame los archivos del directorio en el que estoy
$ cd algo //cambia el directorio a la subcarpeta algo
```
