---
Title: git configuración
Description:
Author: kreilly
Date: 2018-12-04
Template: index
---

## Configuración.

Estos pasos sólo tendremos que realizarlos una sola vez por pc.

Como git está pensado para proyectos colaborativos, para poder hacer un seguimiento de las contribuciones de cada usuario al proyecto final, primero tendremos que decirle quiénes somos.

```
$ git config --global user.name "el nombre que quiera ponerme"
```

Y nuestra dirección de correo electrónico

```
git config --global user.email mi@correo.com
```

Si lo has instalado en windows o mac ya habrás elegido un editor de texto para poner los comentarios a los "commit", es decir, los comentarios que añadiremos a cada fotografía para poder localizarlas mejor. Pero en linux tendremos configurar el editor por defecto si no queremos encontrarnos con Vim.

```
git config --global core.editor "nano"
```
Yo recomendaría nano. No tiene más particularidad que sales del comentario con ctrol+x, intro para confirmar el nombre del archivo a guardar y listo. Si te gusta emacs o vim y te manejas bien en ellos puedes seleccionarlos con el comando anterior eligiendo el que prefieras.
