---
Title: git empezando
Description:
Author: kreilly
Date: 2018-12-04
Template: index
---

## Empezando.

Bien, ya estamos en la carpeta de nuestra novela. Aquí empieza el viaje, todo lo que será.

Para iniciar git en nuestra carpeta, para que pueda empezar a hacer su trabajo, deberemos teclear en el terminal

```
$ git init
```
Veremos cómo se ha creado una carpeta con el nombre .git dentro de nuestro directorio. Ahí empieza la magia.

### Los tres estados.

En el directorio de nuestro proyecto, y en lo que respecta a git hay tres tipos de archivos.

1. Archivos que no están siendo seguidos por git o con cambios que todavía no han sido fijados para la foto (pendientes de git add).
2. Archivos que han sido fijados y que tienen cambios sobre versiones ya fotografiadas (pendientes de git commit).
3. Archivos que forman parte de una foto y no tienen cambios sobre versiones ya fijadas (a la espera de futuros cambios).

Los comandos que necesitaremos son, de nuevo, tres:

```
$ git add 
$ git commit 
$ git status
```

Git add cumple dos funciones. La primera es añadir archivos al seguimiento de git. Git nos informará de cuándo han cambiado a partir de añadirlos.

Podemos añadir un archivo estribiendo:

```
$ git add nombre-del-archivo
```

Si queremos directamente añadir todos los archivos de la carpeta, escribiremos:

```
$ git add .
```

La segunda funcion es preparar los archivos para la foto, pero no la tomará. Cuando ejecutamos git add para un archivo, git fijará su estado de cara al commit (tomar la foto). Si después seguimos modificándolo y más tarde hacemos un commit, la foto no recogerá los cambios que hemos hecho desde el último add. Si queremos saber el estado de nuestros archivos, podemos teclear:

```
$ git status
```

![gitstatus](%base_url%/assets/git/gitstatus.png)

Y la información que nos da es que tenemos un archivo (terminal.md) que ha sido preparado para la foto (added) y está a la espera de la misma (commit), y otro que no ha sido preparado. 

Normalmente añadimos (add) unos archivos y no otros en función de la revisión que queremos fijar. Imaginemos que tenemos unas fichas de personaje y los capítulos de la historia, y aunque hemos ido modificando todo a la vez queremos fijar un cambio sólo con las fichas de personaje, y acto seguido otra con los capítulos. Lo ideal entonces es hacer git add personaje 1, git add personaje 2, etc y después git commit. 

Git commit nos abrirá un editor de texto para anotar un comentario sobre la foto, añadiremos "Fichas de personajes terminadas", por ejemplo, haremos entonces add sobre los capítulos y volveremos a hacer commit para reflejar el comentario de los cambios que hemos hecho en ellos.

O, como es mi caso, al final del día simplemente hago 

```
$ git add .
$ git commit
```

o más vago aún

```
$ git commit -a
```

que junta en uno solo los dos comandos anteriores, y escribo el comentario de lo que he hecho en el día, el cambio mayor localizable y después los detalles:

```
He matado al pasajero pesao

Solucionada la falta de ritmo en los primeros diálogos
Abierta una opción para que el protagonista explote
Odio el capítulo 2 ¿puedes hacer algo, tarao?
```

O puedo ser incluso todavía más vago y escribir:

```
$ git commit -a -m "He matado al pasajero pesao, primeros diálogos ok"
```

Que hace una versión combinada y cutre de todo lo anterior. -m nos sirve para introducir un comentario y que git no nos abra el editor de texto.

Lo importante es utilizar referencias personales para poder localizar lo que necesitas en un futuro. Y divertirse un rato tampoco está mal. Estoy aprendiendo, podéis haceros una idea de lo que suelo utilizar viendo el [registro del repositorio](https://gitlab.com/kreilly/r.a.s.p.i/commits/master) de esta web.

En resumen, después de un día de trabajo sólo tendría que introducir una linea de texto en el terminal para dejar todo registrado y fijado para poder volver cuando quiera. 

Una línea. 
