---
Title: Apuntes git
Description: Las cosas que no suelo recordar.
Author: kreilly
Date: 2018-12-03
Template: index
---

## Lo que mi cabeza no recuerda.

1. Descartar cambios locales frente al repositorio remoto.

```
$ git fetch --all
$ git reset --hard origin/master //si es master la rama
```


