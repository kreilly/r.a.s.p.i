---
Title: XMPP
Description: XMPP con prosody
Author: kreilly
Date: 2018-11-22
Template: index
---

## XMPP con Prosody

> 2018/11/22 Copiado de [la traducción](https://homebrewserver.club/configuring-a-modern-xmpp-server-es.html) de la [guía de homebrewserver.club](https://homebrewserver.club/drafts/configuring-a-modern-xmpp-server.html)  
> 2020/01/15 [Actualizado y en español](https://homebrewserver.club/configuring-a-modern-xmpp-server-es.html) en [homebrewserver.club](https://homebrewserver.club)

### Introducción

Esta es una guía para configurar un servidor de mensajería instantánea basado en XMPP moderno, enfocado a la seguridad, mensajería móvil y fácil de usar. La guía asume que usas ‘Debian Stable’ en el servidor, que quieres alojar y administrar la mensajería instantánea para tu grupo de amigos y que tienes conocimiento básico de usar la línea de comandos en Linux.

### Firewall y DNS

Abre los siguientes puertos en tu firewall para poder comunicarte con el servidor:

> 5000 (hacer de proxy para intercambiar archivos grandes entre clientes)  
5222 (para comunicación entre cliente y servidor, C2S)  
5269 (para comunicación entre servidores, S2S)  
5281 (el puerto https por defecto de prosody)  

También asegúrate de que tienes un dominio con DNS A-records para los siguientes subdominios

> myserver.org (el dominio principal)  
muc.myserver.org (para sala de grupos)  
dump.myserver.org (para componente HTTP-Upload)  
proxy.myserver.org (para el proxy de transferencia de archivos)  

Esta guía usa los dominios escritos arriba, pero puedes ser más creativo :)

### Habilitar HTTPS

Primero, conseguimos un certificado HTTPS firmado por Let’s Encrypt:

Se necesita para XMPP moderno, certificados auto-firmados no funcionaran.

Instala Certbot y consigue los certificados para tu dominio (reemplaza myserver.org por el tuyo):

```
$ sudo apt-get update && sudo apt-get install certbot
$ certbot certonly -d myserver.org -d muc.myserver.org -d dump.myserver.org -d proxy.myserver.org
```

Si funciona, deberías poder ver algo así:

- Congratulations! Your certificate and chain have been saved at
 /etc/letsencrypt/live/myserver.org/fullchain.pem. Your
 cert will expire on 2019-02-15. To obtain a new or tweaked version
 of this certificate in the future, simply run certbot again. To
 non-interactively renew *all* of your certificates, run "certbot
 renew"

### Instalar y configurar Prosody, el servidor XMPP

Instala la version Prosody 0.11 y sus dependencias desde el repositorio oficial de Prosody:

```
$ echo deb http://packages.prosody.im/debian $(lsb_release -sc) main | sudo tee -a /etc/apt/sources.list
$ wget https://prosody.im/files/prosody-debian-packages.key -O- | sudo apt-key add -
$ sudo apt-get update && sudo apt-get install prosody lua-sec
```

Instala los plugins más recientes de Prosody:

```
$ apt-get install mercurial
$ cd /usr/src
$ hg clone https://hg.prosody.im/prosody-modules/ prosody-modules
```

Haz una copia de seguridad de la configuración de Prosody por defecto e instala la de homebrewserver.club

```
$ cd /etc/prosody
$ cp prosody.cfg.lua prosody.cfg.lua.original
$ wget https://homebrewserver.club/downloads/prosody.0.11.cfg.lua -O prosody.cfg.lua
```

La configuración de homebrewserver.club

```
-- una configuración de prosody enfocado a la seguridad, mensajería móvil y fácil de usar.
-- proporcionada por homebrewserver.club
-- el archivo de la configuración original(prosody.cfg.lua.original) tendrá mas información

plugin_paths = { "/usr/src/prosody-modules" } -- el directorio de los plugin no estándar para mantenerlos al dia con mercurial

modules_enabled = {
                "roster"; -- Permite a los usuarios tener una lista de contactos.  Recomendado ;)
                "saslauth"; -- Autenticación entre clientes y servidores. Recomendado si quieres iniciar sesión.
                "tls"; -- Permite conexiones c2s/s2s seguras con TLS
                "dialback"; -- Permite s2s dialback
                "disco"; -- Descubrir servicios entre servidores y clientes
                "private"; -- Almacenamiento XML privado (para guardar las salas a las que te has unido)
                "vcard4"; -- Perfiles de usuarios (guardado en PEP)
                "vcard_legacy"; -- Convierte entre legacy vCard y PEP Avatar, vcard
                "version"; -- Contesta a las peticiones de la versión del servidor
                "uptime"; -- Informa sobre cuánto tiempo ha estado funcionando el servidor
                "time"; -- Permite conocer la hora en el servidor 
                "ping"; -- Contesta XMPP pings con pongs
                "register"; --Permite registrar una cuenta en tu servidor desde un cliente
                "pep"; -- Entre otras cosas, permite a usuarios publicar sus claves OMEMO publicas
                "carbons"; -- XEP-0280: Message Carbons, sincroniza mensajes entre dispositivos
                "smacks"; -- XEP-0198: Stream Management, mantiene conversaciones incluso cuando la red se cae
                "mam"; -- XEP-0313: Message Archive Management, permite descargar el historial de conversasiones del servidor
                "csi_simple"; -- XEP-0352: Client State Indication
                "admin_adhoc"; -- Permite la administración del servidor desde un cliente XMPP (que soporte comandos ad-hoc)
                "blocklist"; -- XEP-0191 bloquear usuarios
                "bookmarks"; -- Sincroniza entre clientes diferentes las salas a las que te has unido
                --"cloud_notify"; -- Soporta XEP-0357 Push Notifications para tener compatibilidad con ChatSecure/iOS. 
                -- iOS normalmente aborta la conexión cuando una aplicación funciona en segundo plano y requiere el uso de los servidores de Push de Apple para levantar la conexión y recibir un mensaje. Habilitar este módulo permite a tu servidor comunicarse con los servidores Push de Apple para ayudar a tus usuarios de iOS.
                -- Sin embargo, lo dejamos comentado porque es un otro ejemplo de una plataforma cloud integrada verticalmente que choca con las ideas de federación y las redes libres. Descomentarlo tiene riesgo de vigilancia de los meta dados de tus usuarios por Apple.
                "server_contact_info"; -- Añade información de contacto en caso de incidencias con el servidor
                };

allow_registration = false; -- Permite registrar una cuenta en tu servidor desde un cliente, para más información visita http://prosody.im/doc/creating_accounts

certificates = "/etc/prosody/certs" -- Ruta donde Prosody busca los certificados: https://prosody.im/doc/letsencrypt
https_certificate = "certs/myserver.org.crt"

c2s_require_encryption = true -- Fuerza a los clientes a usar conexiones cifradas

s2s_secure_auth = true -- Fuerza la autenticación de certificados para conexiones entre servidores

pidfile = "/var/run/prosody/prosody.pid"

authentication = "internal_hashed"

-- Historial de conversaciones 
-- Si mod_mam esta activo, Prosody guardara una copia de cada mensaje. 
-- Se usa para sincronizar conversaciones entre múltiples clientes, incluso
-- si están desconectados. Esta configuración controla cuanto tiempo Prosody
-- guarda los mensajes en el historial antes de eliminarlos.

archive_expires_after = "1w" -- Eliminar el historial de mensajes en una semana

log = { -- descomenta para mayor privacidad
    info = "/var/log/prosody/prosody.log"; -- Cambia 'info' por 'debug' para un registro más detallado
    error = "/var/log/prosody/prosody.err";
    "*syslog";
}

VirtualHost "myserver.org"

-- Habilita http_upload para permitir compartir imágenes entre diferentes dispositivos y diferentes clientes
Component "dump.myserver.org" "http_upload"

-- Permite crear salas 
Component "muc.myserver.org" "muc"
modules_enabled = { "muc_mam", "vcard_muc" }

-- Inicia un proxy para intercambiar archivos grandes entre clientes
Component "proxy.myserver.org" "proxy65"
```

Reemplaza el dominio de ejemplo con tu dominio en el archivo de configuración:

```
$ sed -i 's/myserver.org/tu dominio/g' prosody.cfg.lua
```

De forma alternativa puedes reemplazarlos a mano. Estan en las lineas 39, 75, 81, 85 de prosody.cfg.lua

Importa los certificados de LetsEncrypt con Prosody:

```
$ prosodyctl --root cert import /etc/letsencrypt/live
``` 

Es posible que recibas un resultado similar:

No certificate for host muc.myserver.org found :(  
No certificate for host dump.myserver.org found :(  
No certificate for host proxy.myserver.org found :(  
Imported certificate and key for hosts myserver.org  

Pero no te preocupes, el ultimo certificado contiene información de todos los subdominios.
Para acabar configura cron para renovar los certificados LetsEncrypt automáticamente

```
sudo crontab -e
```

Añade al final:

```
:::console
0 4 0 * 0  /usr/bin/certbot renew --renew-hook "prosodyctl --root cert import /etc/letsencrypt/live" --quiet
```

Esta configuración comprueba y renueva los certificados cada domingo a las 04:00.

Cuando hayas hecho todo esto es hora de arrancar el servidor:

```
$ /etc/init.d/prosody restart
```

Se pueden añadir usuarios desde la línea de comandos. Te pedirá una contraseña:

```
$ prosodyctl adduser me@myserver.org
```

De otra forma puedes cambiar allow_registration = false; a allow_registration = true; en la configuración (linea 35) para permitir a los usuarios registrarse en tu servidor desde sus propios clientes.

Ahora puedes intentar conectarte a tu servidor usando un cliente como Gajim o Conversations. Inicia sesión con tu nombre y contraseña.

Si tienes preguntas sobre Prosody, la [documentación](http://prosody.im/doc) del proyecto es bastante buena (pero en Ingles). Si eso no es suficiente, pregunta los mismos desarrolladores en [la sala XMPP de Prosody](xmpp://prosody.conference.prosody.im?join)






  
  


