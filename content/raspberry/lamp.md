---
Title: LAMP
Description: Apache, PHP y MySQL.
Author: kreilly
Date: 2018-09-21
Template: index
---

## Preparando el servidor.

### 1. Apache.

Instalar con 

```
$ sudo apt-get install apache2 -y
```

Ahora en la dirección de no-ip que hayáis elegido os aparecerá la página de apache.

Quizá tengáis que añádir pi al grupo www-data para poder modificar los archivos.

```
sudo chown -R pi:www-data /var/www
``` 

### 2. PHP.

Instalar con 

```
$ sudo apt-get install php -y
```

> Puedes ver cómo instalar la versión 8.0 [aquí](php8).

### 3. MariaDB.

Instalar y reiniciar apache.

```
$ sudo apt-get install mariadb-server -y
$ sudo service apache2 restart
```

### 3 y medio. Instalar phpMyAdmin.

Prefiero utilizar el terminal ([chuleta](https://gist.github.com/hofmannsven/9164408)), pero para un momento puntual se puede instalar y desinstalar.

```
$ sudo apt-get install phpmyadmin 
$ sudo service apache2 restart
```

Elegiremos apache cuando nos lo pregunte.

No te permite entrar con el usuario que hemos creado en la instalación, así que hay que crear otro:

```
$ sudo mysql --user=root mysql

$ CREATE USER 'usuarioquequeremos'@'localhost' IDENTIFIED BY 'tu_contraseña';
$ GRANT ALL PRIVILEGES ON *.* TO 'usuarioquequeremos'@'localhost' WITH GRANT OPTION;
$ FLUSH PRIVILEGES;
```

Control+D para salir.

Ahora ya podemos entrar a echar un vistazo.

```
https://tu_dominio_o_IP/phpmyadmin
```

El archivo en nuestra IP de apache es el index.html en /var/www/htdocs, si queremos dejar de verlo lo eliminamos

```
sudo rm /var/www/html/index.html
```

> [El siguiente paso](certificados) es conseguir un certificado SSL para el dominio.



