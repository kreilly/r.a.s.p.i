---
Title: mumble
Description: instalando mumble en la raspi.
Author: kreilly
Date: 2018-09-21
Template: index
---

## Mumble.

Instalamos mumble.

```
sudo apt-get install mumble-server
```

Lo configuramos.

```
sudo dpkg-reconfigure mumble-server
```

Te da una serie de opciones

Autostart, por qué no.  
High Priority, con la cantidad de veces que voy a usarlo, por qué no.  
SuperUser, estableces la contraseña de super usuario.  

Configuramos otro par de cosas del servidor de mumble:

```
sudo nano /etc/mumble-server.ini
```

El texto de bienvenida (WelcomeText)  
serverpassword si queremos que la tenga  
Quita la almohadilla en registerserver para ponerle un nombre al canal root.

Reinicias el servidor para que los cambios se efectúen.

```
sudo /etc/init.d/mumble-server restart
```

Abre mumble y configura un nuevo acceso, donde Address es tu dominio, el puerto es 64738 (que ya abrimos cuando abrimos los puertos), el usuario SuperUser y la contraseña la que configuramos antes.
