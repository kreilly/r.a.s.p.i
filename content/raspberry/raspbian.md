---
Title: preparando la raspi
Description: instalando el SO
Author: kreilly
Date: 2018-09-21
Template: index
---

## Preparando la raspi.

### 1. Instalando el SO.

En la página de [descargas](https://www.raspberrypi.org/downloads/raspbian/) de Raspberry Pi puedes descargar la versión de Raspbian que prefieras. Raspbian es una versión de Debian adaptada para su uso en la Raspberry Pi.

La versión lite no trae modo gráfico. Yo siempre había instalado la que incluye el escritorio, pero hemos venido a jugar.

Para quemar la tarjeta puedes utilizar [Etcher](https://etcher.io/). Una vez hecho, el proceso de instalación no es más que meterla en la ranura y enciender la raspi.

Al terminar es bueno actualizar, y muy recomendable antes de hacer cualquier tipo de instalación.

> El teclado no está configurado todavía en español, puedes usar el - del teclado numérico o la ' del teclado.

```
$ sudo apt-get update
$ sudo apt-get upgrade
```
<!-- ya no recomendado
De cuando en cuando también conviene actualizar el firmware.

```
$ sudo rpi-update
```
-->

Ahora vamos a configurar un par de parámetros de la tarjeta, tecleamos

```
$ sudo raspi-config
```

Y veremos lo siguiente:

![raspi-config](%base_url%/assets/raspberry/raspbian/raspi-config.png)

En el 1 cambiaremos la contraseña del usuario pi, que por defecto es "raspberry".

En el 2 escogeremos el nombre del servidor.

En el 4 escogeremos teclado y el idioma.

En el 5 activaremos el ssh.

> Si preferimos hacerlo en la interfaz gráfica hacemos click en el botón de inicio, preferences, raspberry pi configuration y configuramos los mismos parámetros.

### 2. WWW.

Los archivos por defecto en el apache de nuestro servidor se colocarán en /var/www/html. A mí me gusta tener la carpeta www montada en un usb, para poder formatear la tarjeta SD sin cargarme los archivos de las web que tenga alojadas en el caso de que pasara algo crítico. 

Como no es necesario, lo separo [en una entrada aparte](wwwusb).

### 3. Hora local.

Voy a utilizar timedatectl. Buscamos nuestra zona horaria

```
$ timedatectl list-timezones
```

Cuando la tenemos, la establecemos.


```
$ timedatectl set-timezone Europe/Madrid
```

Comprobamos que todo está correcto

```
$ timedatectl
```

### <a id="IpEstatica"></a>4. IP estática. ###

Dentro de la red de nuestra casa la raspi tiene que tener una IP estática por varios motivos (acceso remoto, destino de puertos...), así que abrimos el terminal y tecleamos:

```
$ sudo nano /etc/dhcpcd.conf
```

Y en el editor añadimos al final las siguientes lineas:

```
interface eth0
inform 192.168.1.11
```

Si para conectar la raspi vamos a utilizar wifi en vez de un cable de red, cambiamos eth0 por wlan0. En este caso yo he fijado 192.168.1.11, pero podéis configurar la que veáis.

**Reiniciamos la raspi**, abrimos un terminal, y tecleamos:

```
$ ifconfig
```

Para confirmar que la IP que hemos marcado es la que tenemos.

![ifconfig](%base_url%/assets/raspberry/raspbian/ifconfig.png)

> [El siguiente paso](ssh) es configurar el acceso ssh y abrir puertos.


