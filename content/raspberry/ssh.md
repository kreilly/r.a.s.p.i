---
Title: SSH
Description: SSH, PuTTY, puertos.
Author: kreilly
Date: 2018-09-21
Template: index
---

## Preparando la red.

### 1. SSH y PuTTY.

Con la IP estática ya podemos olvidarnos del teclado, el ratón y el monitor de la raspi y empezar a trabajar desde nuestro pc habitual. En linux y Windows 10 es muy sencillo, simplemente tendremos que abrir un terminal y escribir ssh usuario@dir_ip.

El usuario es el que hayamos elegido, el que viene por defecto es pi. Si la IP estática que hemos elegido es 192.168.1.15 (en el ejemplo anterior era el 192.168.1.11, que no os despiste), escribiremos:

```
$ ssh pi@192.168.1.15
```

Desde Windows 7 instalaremos [PuTTY](https://www.putty.org/), lo abrimos y rellenamos IP, puerto, y si no queremos volverlo a escribir cuando lo abramos la próxima vez lo salvamos.

![putty](%base_url%/assets/raspberry/ssh/putty.png)

Pinchamos en Open y entraremos en una sesión a nuestra raspi. Pero para eso...

### 2. Abriendo puertos.

Abrir puertos depende del router, así que tocará hacer algún trabajo de investigación para abrir el 22 (ssh), el 80 (http), el 443 (https) y ya que estamos el 64738 para la futura instalación de mumble.

En el mío es así

![puertos](%base_url%/assets/raspberry/ssh/puertos.png)

Lo hacemos uno a uno, rango de 22 a 22, protocolo TCP/UDP, dirección IP la de nuestra raspi. Y así con los demás.


> [El siguiente paso](dynudns) es gestionar que ip dinámica se asocie a un dominio.

