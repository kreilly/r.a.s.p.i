---
Title: batocera linux
Description: 
Author: kreilly
Date: 2018-12-10
Template: index
---

## Batocera.

> SSH usuario por defecto: root. Contraseña: linux. Puerto: 22.

Lo [descargamos](https://batocera.org/download) de la web, lo tostamos con [Etcher](https://www.balena.io/etcher/) y a la raspi. Lo único que tendremos que configurar dentro de opciones es la conexión wifi para conectarnos vía ssh o ftp y meter juegos. En la misma ventana de configuración, una vez conectados, nos dirá la ip, y el usuario y la contraseña por defecto son root y linux respectivamente.

Tendremos que meter cada rom en la carpeta del emulador correspondiente. No recuerdo exactamente dónde estaba la carpeta "roms", pero sé que no tardé nada en encontrarla, así que no está muy perdida. 

Para conectar con ella, si no queremos meter los juegos directamente en la tarjeta, podemos mirar en la configuración de la red  —dentro de menú—, la ip, y podrémos conectar a través de ssh utilizando root como usuario, linux como contraseña y el puerto 22. También podremos utilizar filezilla conectando como sftp, por supuesto con los mismos datos.
