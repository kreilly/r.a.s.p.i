---
Title: netdata
Description: 
Author: kreilly
Date: 2018-12-10
Template: index
---

## Netdata

![](%base_url%/assets/raspberry/netdata/netdata.png)

Un montón de estadísticas y de información en vivo sobre nuestra raspi. 

Para instalar nos aseguramos de que nuestro shell es bash

```$ bash```

e instalamos desde github:

```$ bash <(curl -Ss https://my-netdata.io/kickstart.sh)```

En el futuro, para actualizar cuando nos dé el aviso, tendréis que teclear:

```$ bash <(curl -Ss https://my-netdata.io/kickstart.sh) --no-updates```

Después sólo tenemos que [abrir el puerto 19999](%base_url%/raspberry/ssh) en nuestro router, e ir a la dirección y el puerto

http://tu.ip:19999

Podéis echarle un vistazo a la de este servidor en

http://r4sp1.nl:19999

