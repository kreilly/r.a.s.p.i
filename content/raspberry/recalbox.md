---
Title: recalbox
Description: instalación y opciones
Author: kreilly
Date: 2019-04-20
Template: index
---

## Recalbox.

![](%base_url%/assets/raspberry/recalbox/inicio.png)
*Pantalla de inicio.*

Para empezar nos bajamos [el archivo de instalación](https://archive.recalbox.com/) y lo grabamos con [etcher](https://www.balena.io/etcher/) en una tarjeta SD. Una vez hecho la metemos en la raspi y la encendemos. Tarda un ratín en arrancar la primera vez, tampoco demasiado.

La versión que he instalado es la 6.0-DragonBlaze, y de momento parece mucho más fluída que [batocera](batocera). 

La primera vez que utilizas un mando te pide configurarlo, vas indicando los botones y listo. 

Después seleccionamos menú, configuración de red y le damos los datos de conexión a nuestra red wifi, si no hemos utilizado un cable de red. La ponemos en español si queremos, damos un paseo por las opciones para dejarla a nuestro gusto.

![](%base_url%/assets/raspberry/recalbox/status.png)
*Uso de la raspi vía interfaz web.*

Tiene una interfaz de gestión vía web, sólo tienes que entrar en la dirección IP de la instalación. Tiene un montón de opciones, desde saber el estado de uso de nuestra raspi hasta subir roms al emulador correspondiente. Y, ohdiomio, una herramienta para hacer capturas de pantalla.

Una vez configurada la red aparece en el explorador de archivos de tu pc, dentro de red, puedes subir roms desde ahí. También puedes utilizar ssh o ftp con el usuario root, tu ip, y la contraseña recalboxroot.

![](%base_url%/assets/raspberry/recalbox/tetris.png)
*Bajo el tetris, lo subo mediante la interfaz web y arranca sin ningún problema.*

Recalbox, al igual que batocera, incluye la instalación de Kodi, así que también puedes utilizar tu raspi para tu música, tus películas y tus series. 

Para subir roms sólo tenemos que hacerlo en /recalbox/share/roms, dentro de la carpeta del emulador que corresponda. Películas en /recalbox/share/kodi/movies. Música en /recalbox/share/kodi/music. 

Tenemos la opción de utilizar un usb para almacenar los archivos en vez de en la tarjeta SD, yendo a menú, configuración de sistema, almacenamiento.

Iba a instalarlo sin más para ver cómo se hacía, pero creo que me quedaré con recalbox una temporada a ver qué tal. 





