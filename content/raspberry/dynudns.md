---
Title: dynu dns
Description: ip dinámica con dynu
Author: kreilly
Date: 2018-11-21
Template: index
---

## [Dynu.com](https://www.dynu.com/)

Después de una muy corta temporada con NO-IP, llevo un tiempo utilizando [Dynu](https://www.dynu.com/).

Se instala utilizando ddclient, que está en los repositorios

```$ sudo apt-get install ddclient```

Configuramos con 

```$ sudo nano /etc/ddclient.conf```

y pegamos y modificamos según nuestros datos:

```
# ddclient configuration for Dynu  
#  
# /etc/ddclient.conf  
daemon=60                                                    # Check every 60 seconds.  
syslog=yes                                                   # Log update msgs to syslog.  
mail=root                                                    # Mail all msgs to root.  
mail-failure=root                                            # Mail failed update msgs to root.  
pid=/var/run/ddclient.pid                                    # Record PID in file.                                        
use=web, web=checkip.dynu.com/, web-skip='IP Address'        # Get ip from server.  
server=api.dynu.com                                          # IP update server.  
protocol=dyndns2                          
login=tu usuario de dynu                                     # Your username.  
password=tu contraseña de dynu                               # Password or MD5/SHA256 of password.  
tu dominio de dynu  
tu segundo dominio de dynu                                   # List one or more hostnames one on each line.  
#MYDOMAIN.COM  
```

Arrancamos el daemon con 

```
/usr/sbin/ddclient -daemon 300 -syslog
```

Yo no he vuelto a necesitar ejecutarlo, pero para no olvidarlo podemos crearnos un alias.

```
$ sudo nano ~/.bashrc
```

Añadimos en el archivo:

```
alias dynu="/usr/sbin/ddclient -daemon 300 -syslog"
```

activamos el alias

```
$ source ~/.bashrc
```

Y de ahora en adelante para iniciar el daemon de ddclient para mantener la ip dinámica actualizada, sólo tendremos que teclear en el terminal

```
$ dynu
```

***
Enlaces:

https://www.dynu.com/DynamicDNS/IPUpdateClient/DDClient  
https://www.manusoft.es/linux/crear-comando-personalizado-para-el-terminal-linux/

> [El siguiente paso](llave-ssh) es mejorar la seguridad de acceso al servidor con una llave-ssh.
