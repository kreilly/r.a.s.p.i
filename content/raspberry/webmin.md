---
Title: Webmin
Description: instalando el gestor webmin
Author: kreilly
Date: 2021-03-17
Template: index
---

## Webmin raspbian buster

Con webmin podremos gestionar servicios, estadísticas y usuarios de nuestro servidor sin tener que hacerlo desde el terminal. Podemos utilizar 2FA para mantener la seguridad del servidor, o bien podemos activarlo sólo cuando lo necesitemos con `sudo service webmin start` y desactivarlo cuando hayamos terminado con `sudo service webmin stop`.

Instalamos wget (lo normal es que ya lo tengamos).

```
sudo sh -c 'echo "deb http://ftp.au.debian.org/debian/ buster main non-free" > /etc/apt/sources.list.d/nonfree.list' 
sudo apt update
sudo apt install wget
```

Abrimos en el router el puerto 10000 (o el que elijamos).

Añadimos la clave de verificación, el repositorio de webmin, actualizamos recursos e instalamos.

```
wget -qO - http://www.webmin.com/jcameron-key.asc | sudo apt-key add - 
sudo sh -c 'echo "deb http://download.webmin.com/download/repository sarge contrib" > /etc/apt/sources.list.d/webmin.list' 
sudo apt update
sudo apt install webmin
```

Una vez que termine tendremos nuestra instalación en nuestro dominio en el puerto 10000 del modo https://nuestro.dominio:10000.
