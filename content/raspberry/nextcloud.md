---
Title: nextcloud
Description: instalando nextcloud en la raspi.
Author: kreilly
Date: 2018-09-21
Template: index
---

## Nextcloud.

Lo instalé y configuré en el curro en ratos muertos, y el [manual de nextcloud](https://docs.nextcloud.com/server/14/admin_manual/installation/source_installation.html) funcionó estupendamente.

### Requisitos previos.

Lo primero que tenemos que comprobar es que tenemos los módulos de php necesarios para ejecutarlo. Buscamos si el módulo está instalado con

```
$ php -m | grep -i <nombre-del-módulo>
```

Si devuelve algún resultado, es que el módulo está instalado. Si no:

```
$ sudo apt-get install php7.0-<nombre-del-módulo>
```

Un ejemplo nunca viene mal

```
$ php -m | grep -i dom
$ sudo apt-get install php7.0-dom
```

> ***Nota***: tengo la versión 7.0 de php instalada. Si no sabes cuál tienes ejecuta
```
$ php --version
```
y sal de dudas.

### Módulos requeridos.

PHP (>= 7.0, 7.1 or 7.2)  
PHP module ctype  
PHP module dom  
PHP module GD  
PHP module iconv  
PHP module JSON  
PHP module libxml (Linux package libxml2 must be >=2.7.0)  
PHP module mbstring  
PHP module posix  
PHP module SimpleXML  
PHP module XMLReader  
PHP module XMLWriter  
PHP module zip  
PHP module zlib  

### Para la base de datos.

PHP module pdo_mysql (MySQL/MariaDB)  

### Módulos recomendados.

PHP module curl (highly recommended, some functionality, e.g. HTTP user authentication, depends on this)  
PHP module fileinfo (highly recommended, enhances file analysis performance)  
PHP module bz2 (recommended, required for extraction of apps)  
PHP module intl (increases language translation performance and fixes sorting of non-ASCII characters)  
PHP module mcrypt (increases file encryption performance)  
PHP module openssl (required for accessing HTTPS resources)  

### Requeridos para aplicaciones en concreto.

(Yo los instalé todos para evitar sorpresas en el futuro)

PHP module ldap (for LDAP integration)  
PHP module smbclient (SMB/CIFS integration, see SMB/CIFS)  
PHP module ftp (for FTP storage / external user authentication)  
PHP module imap (for external user authentication)   

### Requeridos para aplicaciones en concreto (opcionales).

PHP module exif (for image rotation in pictures app)  
PHP module gmp (for SFTP storage)  

### Opcionales para mejorar el rendimiento del server.

PHP module memcached  

### Módulos para generar vistas previas (opcionales).

PHP module imagick  
avconv or ffmpeg  
OpenOffice or LibreOffice  

Libre office viene instalado con la raspi (en la versión no lite), y para instalar ffmpeg pues el rollo de siempre

```
sudo apt-get update ffmpeg
```

### Para el proceso de la línea de comandos

PHP module pcntl (enables command interruption by pressing ctrl-c)  

### Instalación

Ejecutamos

```
sudo snap install nextcloud
```

Nos bajamos el nextcloud desde [su página](https://nextcloud.com/install/), verificamos la integridad (buscar en [su manual](https://nextcloud.com/install/#), que a mí se me hace bola y ya está allí) y lo metemos en la carpeta en la que queramos instalarlo. Descomprimimos

```
$ unzip nextcloud-x.y.z.zip
```

Sustituye x.y.z por la versión que hayas bajado (completar con tabulador hará que ni te des cuenta de esto).  
Creamos el archivo de configuración de apache

```
$ sudo nano /etc/apache2/sites-available/nextcloud.conf
```
Y pegamos en él

```
Alias /nextcloud "/var/www/nextcloud/"

<Directory /var/www/nextcloud/>
  Options +FollowSymlinks
  AllowOverride All

 <IfModule mod_dav.c>
  Dav off
 </IfModule>

 SetEnv HOME /var/www/nextcloud
 SetEnv HTTP_HOME /var/www/nextcloud

</Directory>
```

Creamos un enlace a sites-enabled

```
$ sudo ln -s /etc/apache2/sites-available/nextcloud.conf /etc/apache2/sites-enabled/nextcloud.conf
```

Activamos los módulos necesarios de apache

```
$ sudo a2enmod rewrite
$ sudo a2enmod headers
$ sudo a2enmod env
$ sudo a2enmod dir
$ sudo a2enmod mime
$ sudo a2enmod setenvif
```

Reiniciamos apache

```
service apache2 restart
```

Damos permisos al usuario HTTP sobre los directorios de nextcloud

```
chown -R www-data:www-data /var/www/html/nextcloud/
```

(Cambia la ruta /var/www/html/nextcloud/ en función de dónde lo hayas instalado)

Crea una nueva base de datos en phpMyAdmin y, finalmente, ve a el directorio en el que lo hayas instalado en un navegador y completa los datos que te solicitan para ponerlo todo en marcha. Básicamente usuario, contraseña y datos de la base de datos que acabas de crear.
