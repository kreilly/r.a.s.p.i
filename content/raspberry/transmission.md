---
Title: transmission
Description: descargando torrents en remoto con transmission
Author: kreilly
Date: 2018-09-23
Template: index
---

## Transmission.

Voy a fusilar sin piedad [la entrada](https://www.atareao.es/tutorial/raspberry-pi-primeros-pasos/transmission-en-raspberry-pi/) del [atareao](https://www.atareao.es), porque me ha funcionado estupendamente.

```
sudo apt update
sudo apt upgrade
sudo rpi-update
```

Instalamos el daemon

`sudo apt install transmission-daemon`

Lo paramos, porque no sirve de nada modificar la configuración cuando está activo

`sudo /etc/init.d/transmission-daemon stop`

Añadimos al usuario "pi" al grupo

`sudo usermod -aG debian-transmission pi`

Creamos los directorios para la descarga, donde queramos

`mkdir -p /srv/torrents/tmp`

Cambiamos /srv/torrents/tmp por el lugar donde queramos que se descargue. Para crear tmp dentro de un directorio tiene que crear previamente el directorio, así que matamos dos pájaros de un tiro.

Cambiamos los permisos del destino

```
sudo chown -R debian-transmission:debian-transmission /srv/torrents
sudo find /srv/torrents -type d -print -exec chmod 775 {} \;
sudo find /srv/torrents -type f -print -exec chmod 664 {} \;
```

Vamos a cambiar el archivo de confirguración, pero antes hacemos una copia del mismo.

```
sudo cp /etc/transmission-daemon/settings.json /etc/transmission.daemon/settings.json.backup
```  

Por otra parte, no me dejó hacerlo desde fuera de la carpeta, así que primero tuve que cambiar con

`cd /etc/transmission-daemon/`

y hacerlo desde ahí sin la ruta completa, es decir

`sudo cp settings.json settings.json.backup`

Funcionó.

Editamos el archivo con nano

`sudo nano /etc/transmission-daemon/settings.json`

y buscamos y modificamos las siguientes cosas

```
"download-dir":"/srv/torrents"
"incomplete-dir":"/srv/torrents/tmp"
"incomplete-dir-enabled":true
"rpc-authentication-required":true
"rpc-bind-address":"0.0.0.0"
"rpc-whitelist-enabled":false
"rpc-enabled":true
"rpc-password":"TUPASSWORD"
```

Ya podemos conectar a IP:puerto (ejemplo 192.168.1.15:9091), o utilizar [tremotesf](https://f-droid.org/en/packages/org.equeim.tremotesf/) en android.

<!--

Hasta ahí el manual de atareao, pero cambiad también

`"rpc-username": "pi"`

porque es el usuario al que le hemos dado permisos más arriba y el usuario por defecto es transmission.

Una cosa es el usuario de conexión a transmission, y otra el que ejecuta el programa, que es pi. No hace falta cambiar el usuario por defecto.

-->







