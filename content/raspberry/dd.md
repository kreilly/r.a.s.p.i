---
Title: Copias de seguridad
Description: Copias de seguridad.
Author: kreilly
Date: 2018-10-19
Template: index
---

# Copias de seguridad 

## Con dd

La instalación de la raspi se monta siempre en las particiones mmcblk0p1 y mcblk0p2, no obstante puedes 
comprobarlo con

```
df -h
```

Una vez localizadas las desmontamos

```
umount /dev/mmcblk0p1
umount /dev/mmcblk0p2
```

Y lanzamos el proceso con dd

```
sudo dd if=/dev/mmcblk0 of=/tus-backups/nombre-que-quieras.dd bs=4M status=progress; sync
```

Para volcar la copia sobre la tarjeta, sólo tendríamos que cambiar "if" y "of"

```
sudo dd of=/dev/mmcbk0 if=/tus-backups/nombre-que-quieras.dd bs=4M status_progress; sync
```

## Con [ImageUSB](https://www.osforensics.com/tools/write-usb-images.html)

***

Para sincronizaciones mirad [Mega](mega)



***Pendiente***  
Muestreo 4M, explicar diferencias.  
¿Se podría hacer en caliente?  
Explicar que p1 y p2 son las particiones.  
Compresiones.  
Verificación con sha1sum

