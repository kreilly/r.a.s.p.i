---
Title: raspberry pi
Description: tutoriales sobre la raspberry pi
Author: kreilly
Date: 2018-09-21
Template: index
---

# [Raspberry Pi](https://www.raspberrypi.org)

**Como servidor**:

[Raspbian, www, IP estática](raspberry/raspbian) 
| [SSH, PuTTY, puertos](raspberry/ssh) 
| [Dynu DNS](raspberry/dynudns)
| [Llave SSH](raspberry/llave-ssh) 
| [Apache, PHP, MySQL](raspberry/lamp) 
| [Certificados SSL](raspberry/certificados) 
| [Wordpress](raspberry/wp) 
| [Nextcloud](raspberry/nextcloud) 
| [Mumble](raspberry/mumble) 
| [WireGuard](raspberry/vpn) 
| [Transmission](raspberry/transmission) 
| [Copias de seguridad](raspberry/dd) 
| [Mega](raspberry/mega) 
| [Mensajería XMPP](raspberry/xmpp) 
| [Netdata](raspberry/netdata)
| [Todo.txt](raspberry/todotxt)
| [SMTP  con Postfix y Gmail](raspberry/smtp)
| [Webmin](raspberry/webmin)

**Como centro multimedia**:

[Kodi](raspberry/kodi) 
| [DVB TV hat](raspberry/tdt) 

**Como emulador**:

Retropie 
| [Recalbox](raspberry/recalbox) 
| [Batocera](raspberry/batocera)
| Lakka
