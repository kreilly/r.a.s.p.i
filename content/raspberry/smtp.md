---
Title: SMTP
Description: SMTP con Postfix
Author: kreilly
Date: 2020-03-03
Template: index
---

# SMTP con Postfix y Gmail.

Actualizamos repositorios, instalamos libsasl2, postfix y mailutils.

```
$ sudo apt-get update
$ sudo apt-get install libsasl2-module
$ sudo apt-get install postfix
$ sudo apt-get install mailutils
```

Cuando salte el menú, en "General type of mail configuration" elegimos "Internet Site", y en "Mail Name" el dominio desde el que queremos que aparezca que se envían los correos (en mi caso r4sp1.nl).

Creamos si no está o configuramos el archivo /etc/postfix/sasl_passwd, añadimos la linea

```
[smtp.gmail.com]:587    username@gmail.com:password
```

Modificándola según nuestros datos. Tendrémos que ir a nuestra cuenta de gmail, seguridad y añadir una contraseña de aplicación para que no necesite 2FA. [Aquí](https://support.google.com/accounts/answer/185833?hl=es) Google explica más.

Cambiamos los permisos del archivo.

```
$ chmod 600 /etc/postfix/sasl_passwd
```

Abrimos /etc/postfix/main.cf y le añadimos 

```
relayhost = [smtp.gmail.com]:587
smtp_use_tls = yes
smtp_sasl_auth_enable = yes
smtp_sasl_security_options =
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt
```
Borramos o marcamos con # la linea "mydestination =" (si existe), lo que permitirá que el sistema lea /etc/aliases (importante más adelante)

Procesamos el archivo de contraseñas para que compile

```
$ postmap /etc/postfix/sasl_passwd
```

Reiniciamos postfix

```
$ sudo service postfix restart
```

Lo probamos

```
mail -s "Test subject" recipient@domain.com
```

Y si hay algún error, podremos ver qué está pasando en /var/log/mail.log

Modificamos /etc/aliases añadiendo:

```
root: usuario@dominio.com
default: usuario@dominio.com
pi: usuario@dominio.com
```

Cambiando usuario@dominio.com por nuestro(s) correo(s). Después, para reconstruir la db 

```
$ newaliases
````

y probamos a enviarnos un correo para ver si lo hace correctamente

```
$ mail root
```



















## Enlaces:

Proceso general:  
https://www.howtoforge.com/tutorial/configure-postfix-to-use-gmail-as-a-mail-relay/  
Instalar libsasl2-modules:   
https://serverfault.com/questions/325955/no-worthy-mechs-found-when-trying-to-relay-email-to-gmail-using-postfix

