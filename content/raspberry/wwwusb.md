---
Title: www en un USB
Description: www en un USB
Author: kreilly
Date: 2020-02-14
Template: index
---

# www en un USB

Podemos ver la configuración de nuestras particiones utilizando blkid.

```
$ sudo blkid
```

Y veremos algo parecido a esto:

```
/dev/mmcblk0p1: LABEL="boot" UUID="3725-1C05" TYPE="vfat" PARTUUID="890a6690-01"
/dev/mmcblk0p2: LABEL="rootfs" UUID="fd695ef5-f047-44bd-b159-2a78c53af20a" TYPE="ext4" PARTUUID="890a6690-02"
/dev/mmcblk0: PTUUID="890a6690" PTTYPE="dos"
/dev/sda1: UUID="b076acf4-936a-4163-b766-1bec9e83df58" TYPE="ext4" PARTUUID="1c18c261-01"
```

Nos quedamos con: 

1. La ubicación **/dev/sda1**.   
2. El formato del pendrive **TYPE="ext4"**.   
3. Con el PARTUUID **1c18c261-01**.  

Si tenéis dudas sobre cuál es vuestro dispositivo, podéis mirar más información con 

```
$ sudo fdisk -l
```

Creamos la carpeta www dentro de var/www

```
sudo mkdir /var/www
```

Montaré sobre el PARTUUID para evitar conflictos en la asignación de sd en el futuro en el caso de arrancar con más usb enchufados, hacemos copia de seguridad y abrimos fstab

```
$ sudo cp /etc/fstab /etc/fstab.backup
$ sudo nano /etc/fstab
```

Veremos algo como esto:

```
proc            /proc           proc    defaults          0       0
PARTUUID=890a6690-01  /boot           vfat    defaults          0       2
PARTUUID=890a6690-02  /               ext4    defaults,noatime  0       1
# a swapfile is not a swap partition, no line here
#   use  dphys-swapfile swap[on|off]  for that
```

y añadiremos la linea para montar siempre el usb en /var/www en el arranque, quedando así:

```
proc            /proc           proc    defaults          0       0
PARTUUID=890a6690-01  /boot           vfat    defaults          0       2
PARTUUID=890a6690-02  /               ext4    defaults,noatime  0       1
PARTUUID=1c18c261-01  /var/www        ext4    defaults          0       0
# a swapfile is not a swap partition, no line here
#   use  dphys-swapfile swap[on|off]  for that
```

Si queréis montar el usb en otra parte, poned otra dirección. Si preferís mantener el usb en otro formato, cambiad ext4 por lo que corresponda.

Antes de reiniciar y ver cómo explota la raspi por un fstab mal formado, teclead

```
$ sudo mount -a
```

Intentará montar todo el contenido de fstab y os avisará si algo da error, si eso sucede podéis volver a modificar fstab para solucionarlo. Si reiniciáis con un fstab que da error pasarán cosas interesantes. 

Si tenéis que formatear el usb podéis utilizar mkfs con el formato mkfs -t type /dev/device, lo que en este caso sería:

```
$ sudo mkfs -t ext4 /dev/sda1
```

o

```
$ sudo mkfs.ext4 /dev/sda1
```