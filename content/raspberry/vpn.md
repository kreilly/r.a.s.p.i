---
Title: VPN
Description: instalando certificados con let's encrypt
Author: kreilly
Date: 2018-09-23
Template: index
---

## VPN con PIVpn y WireGuard

Para lo que queremos hacer con transmission-daemon y para, en general, tener acceso a la red local desde el móvil cuando no estamos en la red wifi, podemos instalar [PiVPN](http://www.pivpn.io) en la raspi y [WireGuard](https://play.google.com/store/apps/details?id=com.wireguard.android&hl=en_US) en el móvil. 

Sólo hace falta un comando 

```
curl -L https://install.pivpn.io | bash
```

Y responder a las preguntas. 

Ip estática la que hemos [configurado](raspbian#IpEstatica).   
Usuario el nuestro de la raspi.  
DNS las que quieras (si dudas las de google, por ejemplo).  
Dirección de acceso dns público y nuestro [dominio](dynudns).

> Nota 13 marzo 2021: el instalador deja todos los .sh en /opt/pivpn en vez de en /opt/pivpn/wireguard y al intentar hacer cualquier cosa da el mensaje da error indicando que no encuentra el .sh. Lo que hice fue entrar en el directorio (cd /opt/pivpn), crear la carpeta (mkdir wireguard) y mover los archivos dentro (sudo mv * wireguard) y funcionó perfectamente.


Después con 

```
pivpn -a
```

Creamos la clave con un usuario y una contraseña que nos generará un certificado .ovpn que se guardará en /home/pi/pivpn, con ```pivpn -qr``` generaremos un código QR que poder escanear desde la aplicación WireGuard en nuestro móvil. Me tomó la IP en vez del dominio, así que la clave sólo me duró hasta que cambió la IP. Se modifica en la misma app de android de WireGuard, modificando el EndPoint del modo domin.io:puerto

Las instrucciones disponibles son 

```
-a,  add              Create a client conf profile
-c,  clients          List any connected clients to the server
-d,  debug            Start a debugging session if having trouble
-l,  list             List all clients
-qr, qrcode           Show the qrcode of a client for use with the mobile app
-r,  remove           Remove a client
-h,  help             Show this help dialog
-u,  uninstall        Uninstall pivpn from your system!
-up, update           Updates PiVPN Scripts
-wg, wgupdate         Updates WireGuard
-bk, backup           Backup VPN configs and user profiles  
```      

Gracias a la VPN podremos ejecutar aplicaciones desde la red móvil como si estuviéramos conectados a la red local.



