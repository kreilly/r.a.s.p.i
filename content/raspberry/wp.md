---
Title: wordpress
Description: instalando wordpress
Author: kreilly
Date: 2018-09-21
Template: index
---

## Wordpress.

La instalación es tan aburrida como tiene que ser (una vez que tenemos instalado apache, php, mysql y phpmyadmin). Creas la base de datos con phpMyAdmin, descargas [wordpress](https://wordpress.org/download/) y lo subes a una carpeta dentro de var/www/html, vas al explorador y escribes la dirección de la carpeta. Te pregunta por el nombre del sitio, el usuario, la contraseña y los datos de la base de datos. Le das a OK, si tiene permisos crea él mismo el config.php y si no te lo da para que lo copies, lo pegues y lo subas.

Hay que modificar los permisos

```
$ sudo usermod -aG www-data user
$ sudo chown -R user:www-data /var/www
$ sudo chmod -R 774 /var/www
```

Con la primera línea añadimos a nuestro usuario (sustituir user por el que sea), con la segunda asignamos al grupo www-data la propiedad de /var/www y con la tercera modificamos los permisos del directorio a 744, suficientes para poder subir actualizaciones desde wordpress.

Para solucionar que nos pida los datos ftp al actualizar, tendremos que añadir al config.php

```
define('FS_METHOD','direct');
```

Hay un par de cosas muy interesantes que podemos hacer para evitar perder datos pase lo que pase, para lo que necesitaremos el manual sobre [mega-cmd](%base_url%/raspberry/mega) y crontab. 

### 1. Copia de seguridad de la base de datos.

Podemos hacer una copia diaria automática de la base de datos, utilizando crontab. 

Abrimos crontab

```
$ crontab -e
``` 

y escribimos al final 

```
59 23 * * * mysqldump -utu-usuario -ptu-contraseña 
el-nombre-de-la-base-de-datos > /directorio-donde-queremos-la-copia-de-seguridad/`date +\%Y\%m\%d`.sql
```

Y eso hará que cada día, a las 23:59, nos haga una copia automáticamente de la base de datos y la guarde en la ubicación seleccionada con el formato AAAAMMDD.sql. Algo así como 20181203.sql.

>Cuidado, eso nos hará copias diarias que se irán acumulando. Si queremos hacer sólo una que se sobreescriba podemos ponerle un nombre fijo que no dependa de la fecha.

### 2. Guardar todo en mega automáticamente usando mega-cmd.

Abrimos mega-cmd

```
$mega-cmd
```

Vamos a quitar que no nos guarde archivos ocultos

```
$ exclude -d .*
```

Y creamos sincronizaciones para la web y las bases de datos.

```
$ sync /directorio-de-nuestro-wordpress /directorio-en-mega-para-guardarlo
$ sync /directorio-donde-tenemos-las-copias-de-seguridad /directorio-en-mega-donde-queremos-guardarlo
```

El directorio en mega tiene que existir de antemano. 

Y con eso tenemos copia de seguridad del contenido del blog y de la base de datos. 

***

**Enlaces**

https://projects.raspberrypi.org/en/projects/lamp-web-server-with-wordpress (en inglés)
