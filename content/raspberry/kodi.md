---
Title: Kodi
Description: Instalación de Kodi en la raspberry pi.
Author: kreilly
Date: 2018-09-21
Template: index
---

## Kodi con LibreElec.

La instalación es muy sencilla. Descargamos la imagen de la sección de [descargas](https://libreelec.tv/downloads/), lo grabamos en la tarjeta SD con [Etcher](https://www.balena.io/etcher/) y la colocamos en la raspi.

Nos hace un par de preguntas acerca de los servicios que queremos activar (ssh, por ejemplo, a mí me parece muy útil).

Seleccionamos los locales de España.

![kodi](%base_url%/assets/raspberry/kodi/1kodi.jpg)

Para controlarlo desde el móvil activamos http. 

![kodi](%base_url%/assets/raspberry/kodi/2kodi.jpg)

Descargamos la aplicación [Kore](https://play.google.com/store/apps/details?id=org.xbmc.kore&hl=en) y la configuramos con el usuario y la contraseña que hemos puesto en el paso anterior.

![kodi](%base_url%/assets/raspberry/kodi/3kodi.png)

La ip podemos encontrarla en ajustes / información del sistema / resumen.

![kodi](%base_url%/assets/raspberry/kodi/4kodi.jpg)

Si queremos fijarla, iremos a las Ajustes / Opciones de librelec / Red y elegimos a la que estemos conectados, botón derecho, editar y le indicamos con qué IP queremos que se conecte.