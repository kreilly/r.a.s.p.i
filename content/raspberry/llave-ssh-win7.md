---
Title: Llave SSH con Putty
Description: Conexión SSH con llaves y cierre de la autentificación por contraseña con Putty.
Author: kreilly
Date: 2020-02-13
Template: index
---

## En Windows 7 con putty.

En windows las llaves están en C:\Users\tuusuario\.ssh

Ya hemos instalado [putty](https://www.putty.org) en el [manual anterior](?raspberry/ssh), ahora vamos a añadirle un par de cosas. En data el usuario:

![putty-data](%base_url%/assets/raspberry/llave-ssh/putty-data.png)

Como prefiere utilizar el formato .ppk, abriremos puttygen, que se instala a la vez que putty.

![putty-gen](%base_url%/assets/raspberry/llave-ssh/putty-gen.png)

En load cargaremos nuestra llave privada (recuerda que está en C:\Users\tuusuario\.ssh, el archivo sin la extensión pub), en generate generaremos el formato de putty, y en save public key guardaremos el formato generado en la misma carpeta en la que tenemos las demás.

Y ahora, en Connection, SSH, Auth la llave pública:

![putty-auth](%base_url%/assets/raspberry/llave-ssh/putty-auth.png)

Y volvemos a sesion para guardar la configuración

![putty-save](%base_url%/assets/raspberry/llave-ssh/putty-save.png)

Copiamos con un editor de texto plano el contenido de id_rsa.pub, abrimos nano y el archivo de registro

`$ sudo nano ~/.ssh/authorized_keys`

y lo copiamos en una linea nueva.

La siguiente conexión ya estará activo.

En windows es un horror, pero es lo que hay.

### Manual.

Siempre podemos abrir en el dispositivo que se quiere conectar id_rsa.pub y copiar el texto, para después pegarlo en una nueva linea del ~/.ssh/authorized_keys de nuestra raspi. Aún así, si utilizamos putty siempre tendremos que indicarle en Connection, SSH, Auth la ubicación de la llave privada.

En todos los casos conectamos de nuevo y comprobamos que no nos pide la password de pi para saber que el proceso está completado.
