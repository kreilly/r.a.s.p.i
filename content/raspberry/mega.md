---
Title: mega
Description: mega-cmd para utilizar nuestra cuenta mega desde el terminal
Author: kreilly
Date: 2018-11-21
Template: index
---

# Mega-Cmd

Me gusta utilizar mega.nz para hacer sincronizaciones puntuales de archivos que me llevaría mucho tiempo recuperar, como el contenido de esta web, por ejemplo. De ese modo tengo la copia operativa, otra en gitlab, otra en mega y otra en el usb instalado en la raspi. 

Mega tiene una versión de su consola especialmente para raspbian en https://mega.nz/cmd, la descargamos e instalamos el .deb

`$ sudo dpkg -i nombre-de-la-descarga-de-mega.deb`

Después podremos acceder a la consola tecleando 

`$ mega-cmd`

![mega-cmd](%base_url%/assets/raspberry/mega/mega-cmd.png)

Podremos entrar en nuestra cuenta tecleando

`$ login nombre-de-usuario contraseña`

Y configurar sincronizaciones con 

`$ sync origen/local/del/archivo ruta/en/mega`

Ver sincronizaciones activas con `sync` sin nada detrás.

Tenemos explicación para más comandos en 

`$ help`

Y para abandonar el cmd de mega sólo tendremos que teclear `quit`.

