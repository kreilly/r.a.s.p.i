---
Title: Llave SSH
Description: Conexión SSH con llaves y cierre de la autentificación por contraseña.
Author: kreilly
Date: 2018-09-25
Template: index
---

## Llave SSH.

> La llave SSH tiene una parte privada que se queda en el cliente (dispositivo que queremos conectar) y un registro público que se queda en el registro autorizado el servidor (dispositivo al que queremos conectarnos).

Tener la conexión SSH abierta a contraseñas hace que podamos recibir un ataque de fuerza bruta.

### Desde el lado del servidor (la raspi).

Primero nos aseguramos de tener openssh-client en la raspi, aunque está.

```
$ apt-get install openssh-client
```

Lo más habitual es que nos diga que ya está en su última versión.

Vamos a crear una llave SSH.

```
$ ssh-keygen
```

Dejamos el directorio por defecto, le ponemos una frase de paso si queremos que nos la pregunte al conectar.

Veremos que en ~/.ssh ahora tenemos dos archivos, la llave pública (id_rsa.pub) y la privada (id_rsa).

### Desde el lado del cliente (el pc).

Primero, si no la tenemos ya para otras cosas (para conectarnos a gitlab, por ejemplo), creamos la llave SSH.

```
$ ssh-keygen
```

Si ya la tenemos creada nos pedirá autorización para sobrescribirla. Sólo es necesaria una llave por dispositivo (podemos tener más, pero por ahora no hace falta y todavía no sé para qué hace falta).

La ubicación la podemos dejar por defecto y nos la guardará en ~/.ssh

Podemos añadirle una frase de paso si queremos que nos la pida al conectarnos.

### Conectándonos desde linux o Win10.

> Si utilizas Windows 7 lee [esta entrada](llave-ssh-win7).

Utilizando la aplicación ssh-copy-id la añadimos al registro de la raspi mediante el terminal.

```
$ ssh-copy-id username@remote_host
```

Cambiando username por nuestro usuario y remote_host por la ip en la que tengamos la raspi.

La primera vez no reconoce la llave porque aún no la tiene registrada, así que nos pedirá la contraseña de pi, y copiará la parte pública de nuestra llave en ~/.ssh/authorized_keys de nuestra raspi.

En ~/.ssh/authorized_keys estarán todos los clientes que hayamos incluído, uno en cada línea (sin contar con el ajuste de linea si está activado).

### Cerrar la autentificación con contraseña.

Una vez seguros de que podemos conectar sin contraseña, abrimos en la raspi la configuración del ssh

```
$ sudo nano /etc/ssh/sshd_config
```

Buscamos la siguiente línea, la desmarcamos (quitamos el #) y cambiamos el yes por no

```
#PasswordAuthentication yes
```

Reiniciamos ssh

```
$ sudo service ssh restart
```

Cuando queramos conectar un nuevo dispositivo tendremos que volver a activarla hasta que tengamos la llave pública en el registro de authorized_keys de la raspi.

> [El siguiente paso](lamp) es instalar Apache, PHP y MySQL.
