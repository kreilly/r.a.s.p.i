---
Title: php8.0
Description: instalar php8.0 en raspbian
Author: kreilly
Date: 2021-03-17
Template: index
---

## php8.0 en [Raspberry Pi OS](https://www.raspberrypi.org/software/)

A la fecha de escribir esta entrada (17 marzo 2021) la versión de PHP soportada por el repositorio oficial es la 7.3, que mantendrá actualizaciones de seguridad hasta finales de este año pero ya no tiene desarrollo. 

> Ver las [versiones con soporte](https://www.php.net/supported-versions.php).

Estos son los pasos para instalar la versión 8.0 en nuestro servidor:

1. Obtener una lista de las extensiones que tenemos actualmente:

```
dpkg -l | grep php | tee packages.txt
```

Para instalar sus versiones en 8.0 teclearemos `apt install php8.0-nombre`, sustituyendo "nombre" por el nombre de la extensión que queramos instalar.

2. Añadir el repositorio de ondrej/php

```
sudo apt install apt-transport-https lsb-release ca-certificates wget -y
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg 
sudo sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
sudo apt update
```

3. Instalar PHP8.0 con CLI y libapache

```
sudo apt install php8.0-common php8.0-cli -y
sudo apt install libapache2-mod-php8.0
```

4. Comprobar que la versión que queremos y los módulos que necesitamos están instalados.

```
php -v
php -m
```

El primero nos dirá la versión de php instalada y activa y el segundo los módulos instalados. 

5. Cargarnos la versión anterior.

```
sudo apt purge '^php7.3.*'
```

7.3 en el caso de este tutorial, si es otra sólo hay que sustituirlo. 




