---
Title: todo.txt
Description: Gestionar las tareas pendientes con todo.txt
Author: kreilly
Date: 2020-02-12
Template: index
---

# Todo.txt

![todotxtCLI](%base_url%/assets/raspberry/todotxt/cap.png)

Empezar a utilizar [todo.txt](http://todotxt.org/) en el terminal utilizando [todo.sh](https://github.com/todotxt/todo.txt-cli). Me gusta la idea de tener un listado de tareas pendientes del servidor para no olvidar ninguna, aunque les siga prestando la misma atención que si lo hiciera.

En la versión del momento en el que escribo ([versiones](https://github.com/todotxt/todo.txt-cli/releases)):

```
$ wget https://github.com/todotxt/todo.txt-cli/releases/download/v2.11.0/todo.txt_cli-2.11.0.tar.gz
$ tar -xzvf todo.txt_cli-2.11.0.tar.gz
$ cd todo.txt_cli-2.11.0.tar.gz
$ chmod +x todo.sh
$ sudo cp todo_completion /etc/bash_completion.d/todo
$ sudo nano ~/.bashrc ^[1]
$ source ~/.bashrc
```

[1] Incluir en .bashrc:
```
# para utilizar t en vez de ./todo.sh cada vez que lo invoquemos
alias t="~/todo.txt_cli-2.11.0/todo.sh" 
# para extender el autocompletar a t
complete -F _todo t 
```

Es sencillo de utilizar y la ayuda en ```t help``` es muy clara.
