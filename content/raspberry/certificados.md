---
Title: Certificados SSL
Description: instalando certificados con let's encrypt
Author: kreilly
Date: 2018-09-21
Template: index
---

## Certificados SSL con Let's Encrypt y CertBot.

Instalamos certbot

```
$ sudo apt install cerbot
```

Ejecutamos el programa

```
$ sudo certbot --apache -d tudominio.com
```

Elegimos redireccionar el http a https.

Creamos una tarea de cron para que lo renueve automáticamente. 

```
$ sudo crontab -e
```

y añadimos 

```
59 4 25 * * /ruta/a/certbot-auto renew
```

Eso lanzara el actualizador cada día 25 a las 4 y 59 de la madrugada. Al gusto.

> [El siguiente paso](wp) es hacer una instalación de WordPress en nuestro servidor.






